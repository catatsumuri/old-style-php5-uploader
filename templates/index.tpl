<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>File Uploader</title>
    <!-- Bulma CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.9.4/css/bulma.min.css">
  </head>
  <body>
    <section class="section">
      <div class="container">
        {if $flashMessage}
        <div class="notification is-primary">
          <button class="delete"></button>
          {$flashMessage}
        </div>
        {/if}

        <div class="box">
          <h1 class="title">
            <a href="index.php">File Uploader</a>
          </h1>
          <form action="index.php?action=save" method="post" enctype="multipart/form-data">
            <div class="file has-name">
              <label class="file-label">
                <input class="file-input" type="file" name="file" required>
                <span class="file-cta">
                  <span class="file-icon">
                    <i class="fas fa-upload"></i>
                  </span>
                  <span class="file-label">Choose a file…</span>
                </span>
                <span class="file-name">No file selected</span>
              </label>
            </div>
            <button class="button is-link" type="submit">Upload</button>
          </form>
        </div>

        {if $files|@count > 0}
        <table class="table is-fullwidth is-striped">
          <thead>
            <tr>
              <th>ファイル</th>
              <th>サイズ</th>
              <th>アップロード日時</th>
              <th>操作</th>
            </tr>
          </thead>
          <tbody>
            {foreach from=$files item=file}
            <tr>
              <td><a href="{$file.url}" target="_blank">{$file.saved_name}</a></td>
              <td>{$file.size} bytes</td>
              {* <td>{$file.size|formatFileSize}</td> *}
              <td>{$file.uploaded_at}</td>
              <td><a class="button is-small is-danger" href="index.php?action=delete&id={$file.id}" onclick="return confirm('本当に削除しますか？');">削除</a></td>
            </tr>
            {/foreach}
          </tbody>
        </table>
        {else}
        <div class="notification is-warning">アップロードされたファイルはありません。</div>
        {/if}
      </div>
    </section>
    <script>
document.addEventListener('DOMContentLoaded', function () {
    // ファイル入力要素を取得
    var fileInput = document.querySelector('.file-input');
    var fileLabel = document.querySelector('.file-name');

    // ファイル入力フィールドが変更されたときのイベントリスナーを設定
    fileInput.addEventListener('change', function (e) {
        // 選択されたファイルのリストを取得
        var files = e.target.files;
        if (files.length > 0) {
            // 最初のファイルの名前を取得して表示
            fileLabel.textContent = files[0].name;
        } else {
            // ファイルが選択されていない場合はデフォルトのテキストを表示
            fileLabel.textContent = 'No file selected';
        }
    });
});
    </script>

  </body>
</html>

