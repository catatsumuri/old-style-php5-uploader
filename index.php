<?php
session_start();
require_once './vendor/autoload.php';
use ByteUnits\Metric;

class SimpleUploader
{
    private $logger;
    private $config;
    private $smarty;
    private $mdb2;

    public function __construct()
    {
        $this->config = new Config_Lite(__DIR__ . '/config/config.ini');
        $logDir = $this->config->get(null, 'log_dir');
        $this->logger = Log::singleton('file', $logDir . '/app.log', 'ident', [
            'mode' => 0600,
            'timeFormat' => '%X %x'
        ]);
        // Smartyインスタンスの初期化
        $this->smarty = new Smarty();
        $this->smarty->template_dir = 'templates';
        $this->smarty->compile_dir = 'templates_c';
        $this->smarty->registerPlugin('modifier', 'formatFileSize', [$this, 'formatFileSize']);

        // データベース接続の初期化
        $dsn = $this->config->get(null, 'db_dsn');
        $this->mdb2 = MDB2::connect($dsn);
        $this->mdb2->setFetchMode(MDB2_FETCHMODE_ASSOC);
        if (PEAR::isError($this->mdb2)) {
            $this->logger->err("データベース接続に失敗しました: " . $this->mdb2->getMessage());
            throw new Exception("データベース接続に失敗しました");
        }
    }

    public function formatFileSize($size)
    {
        return Metric::bytes($size)->format();
    }

    private function display()
    {
        $flashMessage = null;
        if (isset($_SESSION['flash_message'])) {
            $flashMessage = $_SESSION['flash_message']; // メッセージを取得
            unset($_SESSION['flash_message']);
        }

        // データベースからアップロードされたファイルのメタデータを取得
        $files = $this->mdb2->queryAll("SELECT * FROM uploaded_files ORDER BY uploaded_at DESC");
        // アップロードされたファイルへの直接リンクを作成
        foreach ($files as &$file) {
            $uploadDir = $this->config->get(null, 'upload_dir'); // -> stored
            $file['url'] = $uploadDir . $file['saved_name']; // 'uploads/'ディレクトリを想定
        }

        $this->smarty->assign('flashMessage', $flashMessage);
        $this->smarty->assign('files', $files);
        $this->smarty->display('index.tpl');
    }

    private function save()
    {
        if (empty($_FILES['file']['name'])) {
            $_SESSION['flash_message'] = 'ファイルが選択されていません。';
            header('Location: index.php');
            exit;
        }

        $this->logger->info('ファイルアップロード処理が開始されました。');
        $uploadDir = $this->config->get(null, 'upload_dir');

        // データベースのExtendedモジュールをロード
        $this->mdb2->loadModule('Extended');

        // トランザクション開始
        $this->mdb2->beginTransaction();
        $fileData = array(
            'original_name' => $_FILES['file']['name'],
            'saved_name'    => basename($_FILES['file']['name']), // 一時的な名前
            'mime_type'     => $_FILES['file']['type'],
            'size'          => $_FILES['file']['size']
        );

        // ファイルメタデータをデータベースに挿入
        $result = $this->mdb2->extended->autoExecute('uploaded_files', $fileData, MDB2_AUTOQUERY_INSERT);
        if (PEAR::isError($result)) {
            $this->mdb2->rollback(); // エラーがあればロールバック
            $this->logger->err("更新処理に失敗しました: " . $result->getDebugInfo());
            throw new Exception($result->getDebugInfo());
        }

        // 挿入されたレコードのIDを取得
        $id = $this->mdb2->lastInsertId('uploaded_files', 'id');

        // ファイル拡張子を取得
        $fileExt = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

        // 保存するファイル名をフォーマット
        $savedName = sprintf('%05d.%s', $id, $fileExt);

        // saved_nameを更新
        $updateResult = $this->mdb2->query("UPDATE uploaded_files SET saved_name = '$savedName' WHERE id = $id");
        if (PEAR::isError($updateResult)) {
            $this->mdb2->rollback(); // エラーがあればロールバック
            $this->logger->err("保存ファイル名の処理に失敗しました: " . $updateResult->getDebugInfo());
            throw new Exception($updateResult->getDebugInfo());
        }

        // ファイルを指定されたディレクトリに保存
        if (!move_uploaded_file($_FILES['file']['tmp_name'], $uploadDir . $savedName)) {
            throw new Exception('Failed to move uploaded file.');
        }

        // トランザクションコミット
        $this->mdb2->commit();

        $this->logger->info('ファイルアップロード処理が正常に終了しました。');
        $_SESSION['flash_message'] = 'ファイルが正常にアップロードされました。';
        header('Location: index.php');
        exit;
    }

    private function delete()
    {
        if (!isset($_GET['id']) || empty($_GET['id'])) {
            die("ID not found");
        }

        // IDを安全に取得する
        $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);

        $file = $this->mdb2->queryRow("SELECT * FROM uploaded_files WHERE id = ". $this->mdb2->quote($id, 'integer'));
        if (!$file) {
            $_SESSION['flash_message'] = '指定されたファイルが見付かりませんでした';
            header('Location: index.php');
        }

        // ファイルをファイルシステムから削除
        $filePath = $this->config->get(null, 'upload_dir') . $file['saved_name'];
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        // データベースからファイル情報を削除
        $sql = "DELETE FROM uploaded_files WHERE id = ". $this->mdb2->quote($id, 'integer');
        $result = $this->mdb2->query($sql);
        if (PEAR::isError($result)) {
            $this->mdb2->rollback(); // エラーがあればロールバック
            $this->logger->err("ファイル削除に失敗しました: " . $result->getDebugInfo());
            throw new Exception($result->getDebugInfo());
        }

        $this->logger->info('ID: '.$id.' ファイル削除処理が正常に終了しました。');
        $_SESSION['flash_message'] = 'ファイルを正常に削除しました。';
        header('Location: index.php');
        exit;

    }

    public function execute()
    {
        $action = isset($_GET['action']) ? $_GET['action'] : null;
        switch ($action) {
            case 'save':
                $this->save();
                break;
            case 'delete':
                $this->delete();
                break;
            default:
                $this->display();
        }
    }

}
$uploader = new SimpleUploader();
$uploader->execute();
